## 简答题

**1、谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值。**

答:
所谓工程化是指遵循一定的标准和规范通过工具提高效率的一种手段，一切以提高效率、降低成本、质量保证为目的的手段都属于工程化。
工程化一般解决了下面几个问题：
（1）脚手架工具
（2）自动化构建系统
（3）模块化打包
（4）项目代码规范化
（5）自动化部署

　

**2、你认为脚手架除了为我们创建项目结构，还有什么更深的意义？**

答:
脚手架可以帮我们快速生成项目，创建项目基础结构,提供项目规范和约定。
例如：相同的组织结构，相同的代码开发范式、相同的模块依赖、相同的工具配置，相同的基础代码。更加利于代码维护与团队开发。

　

　

## 编程题

**1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具**
实现过程：
 1. 通过命令行交互询问用户问题
 2. 根据用户回答的结果生成文件
 
自定义小型脚手架过程<br>
1、生成package.json 并增加 "bin" :cli.js

2、编写cli.js

```  const fs = require('fs')
  const path = require('path')
  const inquirer = require('inquirer')
  const ejs = require('ejs')
  
  inquirer.prompt([
    {
      type: 'input',
      name: 'name',
      message: 'Project name?'
    }
  ])
  .then(anwsers => {
    // console.log(anwsers)
    // 根据用户回答的结果生成文件
  
    // 模板目录
    const tmplDir = path.join(__dirname, 'templates')
    // 目标目录
    const destDir = process.cwd()
  
    // 将模板下的文件全部转换到目标目录
    fs.readdir(tmplDir, (err, files) => {
      if (err) throw err
      files.forEach(file => {
        // 通过模板引擎渲染文件
        ejs.renderFile(path.join(tmplDir, file), anwsers, (err, result) => {
          if (err) throw err
  
          // 将结果写入目标文件路径
          fs.writeFileSync(path.join(destDir, file), result)
        })
      })
    })
  })
```

3、npm link

4 运行模块名

　

　

**2、尝试使用 Gulp 完成项目的自动化构建**  ( **[先要作的事情](https://gitee.com/lagoufed/fed-e-questions/blob/master/part2/%E4%B8%8B%E8%BD%BD%E5%8C%85%E6%98%AF%E5%87%BA%E9%94%99%E7%9A%84%E8%A7%A3%E5%86%B3%E6%96%B9%E5%BC%8F.md)** )

(html,css,等素材已经放到code/pages-boilerplate目录)

一、初始化项目

1、初始化 package.json

npm init --y

2、集成gulp插件

npm i gulp -D

npm i gulp-load-plugins -D

npm i gulp-babel gulp-clean-css gulp-clean-css gulp-if gulp-imagemin gulp-swig gulp-uglify  gulp-useref gulp-htmlmin -D

3、清理编译的文件夹del

npm i del -D

4、编译sass

npm i sass -D

npm i gulp-sass -D

5、编译ES+

npm i @babel/core @babel/preset-env -D

本地服务器 

npm i browser-sync -D


　
二、配置文件 gulpfile.js


```const { src, dest, parallel, series, watch } = require('gulp')

const del = require('del')
const browserSync = require('browser-sync')

const loadPlugins = require('gulp-load-plugins')

const plugins = loadPlugins()
const bs = browserSync.create()

var sass = require('gulp-sass')(require('sass'));

const data = {
  menus: [
    {
      name: 'Home',
      icon: 'aperture',
      link: 'index.html'
    },
    {
      name: 'Features',
      link: 'features.html'
    },
    {
      name: 'About',
      link: 'about.html'
    },
    {
      name: 'Contact',
      link: '#',
      children: [
        {
          name: 'Twitter',
          link: 'https://twitter.com/w_zce'
        },
        {
          name: 'About',
          link: 'https://weibo.com/zceme'
        },
        {
          name: 'divider'
        },
        {
          name: 'About',
          link: 'https://github.com/zce'
        }
      ]
    }
  ],
  pkg: require('./package.json'),
  date: new Date()
}

const clean = () => {
  return del(['dist', 'temp'])
}

const style = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(plugins.babel({ presets: ['@babel/preset-env'] }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src('src/*.html', { base: 'src' })
    .pipe(plugins.swig({ data, defaults: { cache: false } })) // 防止模板缓存导致页面不能及时更新
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

const image = () => {
  return src('src/assets/images/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

const font = () => {
  return src('src/assets/fonts/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

const extra = () => {
  return src('public/**', { base: 'public' })
    .pipe(dest('dist'))
}

const serve = () => {
  watch('src/assets/styles/*.scss', style)
  watch('src/assets/scripts/*.js', script)
  watch('src/*.html', page)
  // watch('src/assets/images/**', image)
  // watch('src/assets/fonts/**', font)
  // watch('public/**', extra)
  watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**'
  ], bs.reload)

  bs.init({
    notify: false,
    port: 2080,
    // open: false,
    // files: 'dist/**',
    server: {
      baseDir: ['temp', 'src', 'public'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}

const useref = () => {
  return src('temp/*.html', { base: 'temp' })
    .pipe(plugins.useref({ searchPath: ['temp', '.'] }))
    // html js css
    .pipe(plugins.if(/\.js$/, plugins.uglify()))
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })))
    .pipe(dest('dist'))
}

const compile = parallel(style, script, page)

// 上线之前执行的任务
const build =  series(
  clean,
  parallel(
    series(compile, useref),
    image,
    font,
    extra
  )
)

const develop = series(compile, serve)

module.exports = {
  clean,
  build,
  develop
}
```

　三、测试

npx gulp build

```
Using gulpfile ~/VSCodeProject/lagou_study/knock-code/demo3/gulpfile.js
 Starting 'build'...
 Starting 'clean'...
 Finished 'clean' after 31 ms
 Starting 'image'...
 Starting 'font'...
 Starting 'extra'...
 Starting 'style'...
 Starting 'script'...
 Starting 'page'...
 gulp-imagemin: Couldn't load default plugin "gifsicle"
 gulp-imagemin: Couldn't load default plugin "gifsicle"
 gulp-imagemin: Couldn't load default plugin "gifsicle"
 Finished 'extra' after 1.97 s
 gulp-imagemin: Minified 1 image (saved 679 B - 5.9%)
 Finished 'page' after 1.92 s
 Finished 'script' after 1.92 s
 Finished 'style' after 1.92 s
 Starting 'useref'...
 gulp-imagemin: Minified 2 images (saved 22.8 kB - 26.5%)
 Finished 'font' after 3.07 s
 Finished 'image' after 3.38 s
 Finished 'useref' after 8.05 s
 Finished 'build' after 10 s
```


## 说明：

本次作业中的编程题要求大家完成相应代码后

- 提交一个项目说明文档，要求思路流程清晰。
- 或者简单录制一个小视频介绍一下实现思路，并演示一下相关功能。
- 说明文档和代码统一提交至作业仓库。
