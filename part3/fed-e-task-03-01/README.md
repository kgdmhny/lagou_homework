## 一、简答题

### 1、当我们点击按钮的时候动态给 data 增加的成员是否是响应式数据，如果不是的话，如何把新增成员设置成响应式数据，它的内部原理是什么。

```js
let vm = new Vue({
 el: '#el'
 data: {
  o: 'object',
  dog: {}
 },
 method: {
  clickHandler () {
   // 该 name 属性是否是响应式的
   this.dog.name = 'Trump'
  }
 }
})
```

 　不是响应式数据

　当创建好 Vue 实例后，新增一个成员，此时 data 并没有定义该成员，data 中的成员是在创建 Vue 对象的时候 new Observer 来将其设置成响应式数据，当 Vue 实例化完成之后，再添加一个成员，此时仅仅是给 vm 上增加了一个js属性而已，因此并不是响应式的

　对于已经创建的实例，Vue不允许动态添加根级别的响应式属性。但是可以使用 Vue.set(object, propertyName, value)方法向嵌套对象添加响应式属性。



### 2、请简述 Diff 算法的执行过程

diff算法是一种通过同层的树节点进行比较的高效算法，避免了对树进行逐层搜索遍历，所以时间复杂度只有 O(n)。

diff算法有两个比较显著的特点：

1、比较只会在同层级进行, 不会跨层级比较。

2、在diff比较的过程中，循环从两边向中间收拢。

diff流程：

首先定义 oldStartIdx、newStartIdx、oldEndIdx 以及 newEndIdx 分别是新老两个 VNode 的两边的索引。

接下来是一个 while 循环，在这过程中，oldStartIdx、newStartIdx、oldEndIdx 以及 newEndIdx 会逐渐向中间靠拢。while 循环的退出条件是直到老节点或者新节点的开始位置大于结束位置。

while 循环中会遇到四种情况：

情形一：当新老 VNode 节点的 start 是同一节点时，直接 patchVnode 即可，同时新老 VNode 节点的开始索引都加 1。

情形二：当新老 VNode 节点的 end 是同一节点时，直接 patchVnode 即可，同时新老 VNode 节点的结束索引都减 1。

情形三：当老 VNode 节点的 start 和新 VNode 节点的 end 是同一节点时，这说明这次数据更新后 oldStartVnode 已经跑到了 oldEndVnode 后面去了。这时候在 patchVnode 后，还需要将当前真实 dom 节点移动到 oldEndVnode 的后面，同时老 VNode 节点开始索引加 1，新 VNode 节点的结束索引减 1。

情形四：当老 VNode 节点的 end 和新 VNode 节点的 start 是同一节点时，这说明这次数据更新后 oldEndVnode 跑到了 oldStartVnode 的前面去了。这时候在 patchVnode 后，还需要将当前真实 dom 节点移动到 oldStartVnode 的前面，同时老 VNode 节点结束索引减 1，新 VNode 节点的开始索引加 1。

while 循环的退出条件是直到老节点或者新节点的开始位置大于结束位置。

情形一：如果在循环中，oldStartIdx大于oldEndIdx了，那就表示oldChildren比newChildren先循环完毕，那么newChildren里面剩余的节点都是需要新增的节点，把[newStartIdx, newEndIdx]之间的所有节点都插入到DOM中

情形二：如果在循环中，newStartIdx大于newEndIdx了，那就表示newChildren比oldChildren先循环完毕，那么oldChildren里面剩余的节点都是需要删除的节点，把[oldStartIdx, oldEndIdx]之间的所有节点都删除


## 二、编程题

### 1、模拟 VueRouter 的 hash 模式的实现，实现思路和 History 模式类似，把 URL 中的 # 后面的内容作为路由的地址，可以通过 hashchange 事件监听路由地址的变化。


```javascript
let _Vue = null

export default class VueRouter {
  static install (Vue) {
    // 1.判断当前插件是否已经被安装
    // 如果插件已经安装直接返回
    if (VueRouter.install.installed && _Vue === Vue) return
    VueRouter.install.installed = true
    // 2.把 Vue 构造函数记录到全局变量
    _Vue = Vue
    // 3.把创建 Vue 实例时候传入的 router 对象注入到 Vue 实例上
    // 混入
    _Vue.mixin({
      beforeCreate () {
        // 判断 router 对象是否已经挂载了 Vue 实例上
        if (this.$options.router) {
          // 把 router 对象注入到 Vue 实例上
          _Vue.prototype.$router = this.$options.router
          this.$options.router.init()
        }
      }
    })
  }

  constructor (options) {
    this.options = options
    // 记录路径和对应的组件
    this.routeMap = {}
    this.data = _Vue.observable({
      // 当前的默认路径
      current: '/'
    })
  }

  init () {
    this.createRouteMap()
    this.initComponents(_Vue)
    this.initEvent()
  }

  createRouteMap () {
    // routes => [{ name: '', path: '', component: }]
    // 遍历所有的路由信息，记录路径和组件的映射
    this.options.routes.forEach(route => {
      // 记录路径和组件的映射关系
      this.routeMap[route.path] = route.component
    })
  }

  initComponents (Vue) {
    _Vue.component('router-link', {
      // 接收外部传入的参数
      props: {
        to: String
      },
      // 使用运行时版本的 Vue.js
      // 此时没有编译器 直接来写一个 render函数
      render (h) { // 参数 h 创建虚拟DOM render函数中调用h函数并将结果返回
        // h函数 接收三个参数
        return h('a', { // 1. 创建的元素对应的选择器
          attrs: { // 2. 给标签设置属性 attes 指明DOM对象属性
            // history
            // href: this.to
            // hash
            href: '#' + this.to
          },
          on: { // 给 a标签 注册点击事件
            click: this.clickhander
          }
        }, [this.$slots.default]) // 3. 生成元素的子元素
      },
      methods: {
        clickhander (e) { // 时间参数 e
          // 改变浏览器地址栏 pushiState 不向服务器发送请求
          // history
          // history.pushState({}, '', this.to) // data title url
          // hash
          window.location.hash = '#' + this.to
          this.$router.data.current = this.to // 响应式对象data
          e.preventDefault() // 阻止事件默认行为
        }
      }
      // template: '<a :href="to"><slot></slot></a>'
    })

    const self = this // 保存 this
    _Vue.component('router-view', {
      render (h) {
        // 根据当前路径找到对应的组件，注意 this 的问题
        const component = self.routeMap[self.data.current]
        return h(component) // 将组件转换为虚拟DOM返回
      }
    })
  }

  // initEvent () {
  //   window.addEventListener('popstate', () => {
  //     this.data.current = window.location.pathname
  //   })
  // }

  // 监听页面 load 和 hashchange 方法，在这个地方有个判断
  // 如果当前页面的 hash 不存在，则自动加上 '#/' ,并加载 '/' 的组件
  initEvent () {
    window.addEventListener('load', this.hashChange.bind(this))
    window.addEventListener('hashchange', this.hashChange.bind(this))
  }

  hashChange () {
    if (!window.location.hash) {
      window.location.hash = '#/'
    }
    this.data.current = window.location.hash.substr(1)
  }
}

```

### 2、在模拟 Vue.js 响应式源码的基础上实现 v-html 指令，以及 v-on 指令。

* v-html 指令与 v-text 指令相似，将 textContent 更改为 innerHTML
  
 ```javascript
 
 class Compiler {
  ...
  // 处理 v-html 指令
  htmlUpdater (node, value, key) {
    node.innerHTML = value
    new Watcher(this.vm, key, (newValue) => {
      node.innerHTML = newValue
    })
  }
}
 ```

* v-on 指令
* 在 vue.js 文件 添加变量 methods，把事件注入到 vue 实例
* 在 compiler.js文件 将on: 修改为空 只保留后面的事件 再处理相应指令

```javascript
class Vue {
  constructor(options) {
    ...
    this.$methods = options.methods || {}
    this._proxyMethods(this.$methods)
    ...
  }
  ...
  // 把methods注入vue实例
  _proxyMethods(methods) {
    Object.keys(methods).forEach(key => {
      this[key] = this.$methods[key]
    })
  }
}

class Compiler {
  update (node, key, attrName) {
    // 删除 on: 前缀
    if (attrName.startsWith('on:')) {
      attrName = attrName.replace('on:', '')
    }
    ...
  }
  ...
  // 处理 v-on 指令 此处举两个例子
  clickUpdater (node, value, key) {
    node.onclick = value
  }
  mouseoverUpdater (node, value, key) {
    node.onmouseover = value
  }
  ...
}
```

测试代码：

```htm
<body>
  <h1>v-on</h1>
  <div v-on:click="clickHandler">点击触发</div>
  <div v-on:mouseover="mouseOver">鼠标进入触发</div>
</body>
<script>
  let vm = new Vue({
    el: '#app',
    data: {
      html: '<p style="color: skyblue">这是一个p标签</p>',
    },
    methods: {
      clickHandler() {
        alert('点击了')
      },
      mouseOver(){
        alert('鼠标进入')
      }
    }
  })
</script>

```

### 3、参考 Snabbdom 提供的电影列表的示例，利用Snabbdom 实现类似的效果，如图：

<img src="images/Ciqc1F7zUZ-AWP5NAAN0Z_t_hDY449.png" alt="Ciqc1F7zUZ-AWP5NAAN0Z_t_hDY449" style="zoom:50%;" />

实现原理：

1. 导入 init 函数注册模块返回一个 patch，导入 h 函数创建 vnode
2. 导入 className 切换类样式、eventlisteners 注册事件、style 行内样式、props 设置DOM元素的属性
3. 当初始的 HTML 文档被完全加载和解析完成之后，使用 h函数 生成 vnode，然后 patch 到 id 为 container 的 div 上生成页面结构
4. 排序按钮通过 eventlisteners 的 on 属性调用排序函数，点击按钮调用排序函数，然后重新渲染页面；class 判断当前的是以什么排序，对应的按钮显示为激活状态；style 设置整个列表的高度
5. 在定义列表的行结构时，使用 props 模块设置 DOM 的 key 属性；通过 style 模块添加 delayed(进场样式) 和 remove(退场样式) 以及 opacity 和 transform; hook 属性中设置了 insert 属性，也就是在节点插入到 dom 中后，触发该回调，设置节点的高度
6. 每一行的最后一列添加删除的图标，使用 eventlisteners 的 on 属性调用删除函数，点击删除按钮删除该行，然后重新渲染页面
7. 添加按钮的回调函数就是随机生成一个数据对象，但是其 rank 属性是全局递增的。然后对新数据调用 h函数 生成新的 vnode，并且 patch 到页面上，重新渲染了页面
   
注意：当删除剩余一条数据的时候会报错 Uncaught TypeError: Cannot read property 'offset' of undefined 是因为当删除仅剩的一条数据是 render 函数中的 data[data.length - 1] 为 undefined，需要对该处代码做判断做对应的处理，此处已改为判断为 undefined 时 将不获取数据 offset 而是赋值为0

```javascript

// package.json
{
  "scripts": {
    "dev": "parcel index.html --open"
  },
  "devDependencies": {
    "parcel-bundler": "1.12.4",
    "snabbdom": "0.7.4"
  }
}
// index.html
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Reorder animation</title>
    <style>
    ...
    </style>
  </head>
  <body>
    <div id="container"></div>
    <script src="./script.js"></script>
  </body>
</html>
// script.js
import { h, init } from 'snabbdom'
import className from 'snabbdom/modules/class'
import eventlisteners from 'snabbdom/modules/eventlisteners'
import style from 'snabbdom/modules/style'
import props from 'snabbdom/modules/props'

let patch = init([className, eventlisteners, style, props])

var vnode

var nextKey = 11
var margin = 8
var sortBy = 'rank'
var totalHeight = 0
var originalData = [...]
var data = [...]

// 根据prop排序
function changeSort(prop) {
  sortBy = prop
  data.sort((a, b) => {
    if (a[prop] > b[prop]) {
      return 1
    }
    if (a[prop] < b[prop]) {
      return -1
    }
    return 0
  })
  render()
}

// 添加一条数据
function add() {
  // 随机获取 originalData 中的一条数据
  var n = originalData[Math.floor(Math.random() * 10)]
  // 添加数据
  data = [{rank: nextKey++, title: n.title, desc: n.desc, elmHeight: 0}].concat(data)
  render()
  render()
}

// 根据传递的movie移除对应的数据
function remove(movie) {
  data = data.filter((m) => { return m !== movie; })
  render()
}

// 定义列表行
function movieView(movie) {
  return h('div.row', { // 定义行
    key: movie.rank,
    // 行内样式
    style: {opacity: '0', transform: 'translate(-200px)',
            // 进场样式
            delayed: {transform: `translateY(${movie.offset}px)`, opacity: '1'},
            // 退场样式
            remove: {opacity: '0', transform: `translateY(${movie.offset}px) translateX(200px)`}},
    hook: {insert: (vnode) => { movie.elmHeight = vnode.elm.offsetHeight; }},
  }, [ // 定义列表列
    h('div', {style: {fontWeight: 'bold'}}, movie.rank),
    h('div', movie.title),
    h('div', movie.desc),
    h('div.btn.rm-btn', {on: {click: [remove, movie]}}, 'x'),
  ])
}

// 调用patch对比新旧vnode渲染
function render() {
  data = data.reduce((acc, m) => {
    var last = acc[acc.length - 1]
    m.offset = last ? last.offset + last.elmHeight + margin : margin
    return acc.concat(m)
  }, [])
  // 处理删除所有内容报错的问题
  totalHeight = data[data.length - 1].offset + data[data.length - 1].elmHeight
  vnode = patch(vnode, view(data))
}

function view(data) {
  return h('div', [
    h('h1', 'Top 10 movies'),
    h('div', [
      h('a.btn.add', {on: {click: add}}, 'Add'),
      'Sort by: ',
      h('span.btn-group', [
        h('a.btn.rank', {class: {active: sortBy === 'rank'}, on: {click: [changeSort, 'rank']}}, 'Rank'),
        h('a.btn.title', {class: {active: sortBy === 'title'}, on: {click: [changeSort, 'title']}}, 'Title'),
        h('a.btn.desc', {class: {active: sortBy === 'desc'}, on: {click: [changeSort, 'desc']}}, 'Description'),
      ]),
    ]),
    h('div.list', {style: {height: totalHeight+'px'}}, data.map(movieView)),
  ])
}

window.addEventListener('DOMContentLoaded', () => {
  var container = document.getElementById('container')
  vnode = patch(container, view(data))
  render()
})
```
